

export default function Banner(){

    return(
    <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1>Welcome to Course Booking App!</h1>
          <p>Opportunities for everyone, everywhere</p>
          <a href="#" className="btn btn-info">Click here</a>
        </div>
    </div>

    )
}