import { useState } from "react"
import { Card, Button } from "react-bootstrap"

export default function CourseCard({courseProp}){
    let {name,description,price}= courseProp
    
    let [count, setCount]= useState(0)
    function handleClick(){
        console.log(seat,count)
        if(seat<=0){
            alert("no more seats")
        }else{
            setCount(++count)
            setSeat(--seat)
        }
    }
    let[seat, setSeat] = useState(30)

    return(
        <Card>
            
            <Card.Body>     
            <Card.Title>{name}</Card.Title>
            <Card.Text>Description:<br />
                {description}
            </Card.Text>
            <Card.Text>Price:<br />
            {price}
            </Card.Text>
            <Card.Text>{count} Enrollees</Card.Text>
            <Button variant="primary" onClick={handleClick}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
