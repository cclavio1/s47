import{Form,Button,Row,Col,Container} from 'react-bootstrap'
import {useState,useEffect} from 'react'

export default function Register(){
    let [fN,setFN] = useState('')
    let [lN,setLN] = useState('')
    let [email,setEmail] = useState('')
    let [pw,setPW] = useState('')
    let [vpw,setVPW] = useState('')
    let [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {
		console.log('render')

		// if all fields are filled out and pw & vpw is equal, change the state to false
		if((fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") && (pw == vpw)){

			setIsDisabled(false)

		} else {
			//if all input fields are empty, keep the state of the button to true
			setIsDisabled(true)
		}

		//listen to state changes: fn, ln, em, pw, vf
	}, [fN, lN, email, pw, vpw])

	const registerUser = (e) => {
		e.preventDefault()

	}

return(
    <Container className="m-5">
            <h3 className='text-center'>Register</h3>
        <Row className="justify-content-center">
            <Col xs={12} md={6}>
            <Form onSubmit={(e)=>registerUser(e)}>
            <Form.Group className="mb-3">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" value={fN} onChange={(e)=>{setFN(e.target.value)}}/>
            </Form.Group>   

            <Form.Group className="mb-3">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" value={lN} onChange={(e)=>{setLN(e.target.value)}}/>
            </Form.Group>  

            <Form.Group className="mb-3">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" value={pw} onChange={(e)=>{setPW(e.target.value)}}/>
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" value={vpw} onChange={(e)=>{setVPW(e.target.value)}}/>
            </Form.Group>
            
            <Button variant="info" type="submit" disabled={isDisabled}>
                Submit
            </Button>
        </Form>
            </Col>
        </Row>
    </Container>
)    
}