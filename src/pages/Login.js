import{Form,Button,Row,Col,Container} from 'react-bootstrap'
import {useState,useEffect} from 'react'


export default function Login(){

    let [email,setEmail] = useState('')
    let [pw,setPW] = useState('')
    let [isDisabled,setIsDisabled] = useState(true)
    
    let login=()=>{
        alert("you are now logged in")
    }

    useEffect((e)=>{
        if(email==""||pw==""){
            setIsDisabled(true)
        }else{
            setIsDisabled(false)
        }
    })

    return(
        <Container className="m-5">
                <h3 className='text-center'>Register</h3>
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                <Form onSubmit={(e)=>login(e)}>

                <Form.Group className="mb-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" value={pw} onChange={(e)=>{setPW(e.target.value)}}/>
                </Form.Group>

                
                <Button variant="info" type="submit" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
                </Col>
            </Row>
        </Container>
    )
}