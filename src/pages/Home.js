
import AppNavBar from '../components/AppNavbar';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
import { Fragment } from 'react';
import Highlights from '../components/Highlights';


export default function Home(){
    return(
        <Fragment>
            <AppNavBar />
            <Banner />
            <Highlights />
            <Footer/>
        </Fragment>
    )
}